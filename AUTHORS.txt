Main Authors
============

Nicolas Piaget <nicolas.piaget@env.ethz.ch>

Code Contributors
=================

Marina Dütsch <marina.duetsch@env.ethz.ch>
Stefan Ruedisuehli <stefan.ruedisuehli@flotzilla.ch>
Denis Sergeev <dennis.sergeev@gmail.com>
Mathias Hauser <mathias.hauser@env.ethz.ch>

