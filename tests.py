# ========================================================================
# ========================================================================
#       Script Usage: 
#         Created by: ---------- Xianglin DAI ----------- 
#                              Ph.D. Candidate 
#                     School of Atmospheric Sciences 
#                     Email: lyniedairce@smail.nju.edu.cn 
#                     ----- Nanjing University ----- 
#                                                    
#  Records of Revisions:                             
#     Date      Programmer    Description of change    
#  ==========   ==========    =====================    
#  03/12/2019   Xianglin DAI        V.1.0                    
# ========================================================================
# |---------------------File Name: tests.py------------------------| 
from datetime import datetime, timedelta
from dypy.small_tools import interval
startdate = datetime(2013, 6, 1, 0)
enddate = startdate + timedelta(days=5)
dates = [(d, d + timedelta(hours=48)) for d in
          interval(startdate, enddate, timedelta(hours=6))]

from dypy.lagranto import LagrantoRun
lrun = LagrantoRun(dates, workingdir='/run/media/xldai/cloud/Data/DATABANK/dataset/Interim',
            outputdir='output', version='ecmwf')
specifier = "'box.eqd(5,20,40,50,20)@profile(850,500,10)@hPa'"
out_create_startf = lrun.create_startf(startdate, specifier, tolist=True)




